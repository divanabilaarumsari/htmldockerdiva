function c()
{
	return confirm("Do you want to submit registration form?");
}

var form = document.getElementById('regisForm');

form.onsubmit=function(event)
{
	document.getElementById("error")
	.innerHTML = "";

	var nameTxt = form["username"].value;
	var gender = form["gender"].value;
	var email = form["email"].value;
	var city = form["city"].value;
	var ideaname = form["ideaname"].value;
	var ideaType= form["ideaType"].selectedIndex;
	var ideaDetails= form["ideaDetails"].value;
	var agreement = form["agreement"].checked;

	if (nameTxt=="")
	{
		document.getElementById("error")
		.innerHTML = "Name must be filled";
		return false;
	}

	if (nameTxt.length<8)
	{
		document.getElementById("error")
		.innerHTML = "Name character must more than 8";
		return false;
	}

	if (nameTxt==" ")
	{
		document.getElementById("error")
		.innerHTML = "Name must consist of 2 words";
		return false;
	}

	if(gender == "")
	{
		document.getElementById("error")
		.innerHTML = "Select your gender male or female";
		return false;
	}

	if(email=="")
	{
		document.getElementById("error")
		.innerHTML = "Email must be filled";
		return false;
	}

	if(city=="")
	{
		document.getElementById("error")
		.innerHTML = "City must be filled";
		return false;
	}

	if(city.value.contains=="city")
	{
		return true
	}
	else{
		document.getElementById("error")
		.innerHTML = "City must in the right format";
		return false;	
	}
	
	if (ideaname=="")
	{
		document.getElementById("error")
		.innerHTML = "Idea name must be filled";
		return false;
	}

	if (ideaname.length<5)
	{
		document.getElementById("error")
		.innerHTML = "Idea Name must more than 5 character";
		return false;
	}

	if(ideaType== 0)
	{
		document.getElementById("error")
		.innerHTML = "Select your Idea Type";
		return false;
	}

	if (ideaDetails=="")
	{
		document.getElementById("error")
		.innerHTML = "Name must be filled";
		return false;
	}

	if (ideaDetails.length<20)
	{
		document.getElementById("error")
		.innerHTML = "Detail idea must more than 20 character";
		return false;
	}

	if (ideaDetails==" ")
	{
		document.getElementById("error")
		.innerHTML = "Name must consist of 5 words";
		return false;
	}
		if(!agreement)
	{
		document.getElementById("error")
		.innerHTML = "You must agree!";
		return false;
	}
	else
	{
		alert("Welcome "+nameTxt);
		return true;
	}
	  
}

